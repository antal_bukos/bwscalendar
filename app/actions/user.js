import {createAction} from 'redux-actions';

export const setUser = createAction('user/SET_USER');
export const clearUser = createAction('user/CLEAR_USER');
