import React, {Component} from 'react';
import {
  NavigatorIOS,
  StyleSheet,
  TabBarIOS
} from 'react-native';
import PropTypes from 'prop-types';

import calendarIcon from './images/calendar-icon.png';
import vacationIcon from './images/vacation-icon.png';
import CalendarView from './views/calendar/CalendarView';
import VacationsView from './views/vacations/VacationsView';
import LoginView from './views/login';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF'
  }
});

class CalendarApp extends Component {
  static propTypes = {
    setUser: PropTypes.func.isRequired,
    clearUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 'calendar'
    };
  }

  render() {
    const {
      user,
      setUser,
      clearUser
    } = this.props;

    if (!user.loggedIn) {
      return (
        <LoginView
          setUser={setUser}
          clearUser={clearUser}
          user={user}
        />
      );
    }
    return (
      <TabBarIOS>
        <TabBarIOS.Item
          title="Calendar"
          selected={this.state.selectedTab === 'calendar'}
          onPress={() => this.setState({selectedTab: 'calendar'})}
          icon={calendarIcon}
          style={styles.container}
        >
          <NavigatorIOS
            style={{flex: 1}}
            initialRoute={{
              component: CalendarView,
              title: 'Calendar'
            }}
          />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Vacation Requests"
          selected={this.state.selectedTab === 'vacation'}
          onPress={() => this.setState({selectedTab: 'vacation'})}
          icon={vacationIcon}
          style={styles.container}
        >
          <NavigatorIOS
            style={{flex: 1}}
            initialRoute={{
              component: VacationsView,
              title: 'Vacation Requests'
            }}
          />
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
}

export default CalendarApp;
