import {connect} from 'react-redux';

import {setUser, clearUser} from './actions/user';

import CalendarApp from './app';

const mapDispatchToProps = (dispatch) => ({
  setUser: (email) => {
    dispatch(setUser(email));
  },
  clearUser: () => {
    dispatch(clearUser());
  }
});

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(CalendarApp);
