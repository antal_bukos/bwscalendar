import {handleActions} from 'redux-actions';
import * as Actions from '../actions/user';

const initialState = {
  email: '',
  loggedIn: false,
  isLoading: true
};

export default handleActions({
  [Actions.setUser]: (state, action) => {
    return {
      ...state,
      email: action.payload,
      loggedIn: true,
      isLoading: false
    };
  },
  [Actions.clearUser]: (state) => {
    return {
      ...state,
      email: '',
      loggedIn: false,
      isLoading: false
    };
  }
}, initialState);
