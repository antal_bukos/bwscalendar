import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  View
} from 'react-native';
import Calendar from 'react-native-calendar';

import Events from './Events';

const styles = StyleSheet.create({
  container: {
    paddingTop: 60
  }
});

const calendarStyling = {
  calendarContainer: {
    backgroundColor: '#FFFFFF'
  },
  hasEventCircle: {
    backgroundColor: '#4CAF50'
  },
  selectedDayCircle: {
    backgroundColor: '#F44336'
  }
};

const MOCK_EVENTS = Array(50).fill({
  project: 'Learning',
  time_spent: '8h'
});

class CalendarView extends Component {
  static propTypes = {
    navigator: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      selectedDate: ''
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Calendar
          showControls
          scrollEnabled
          showEventIndicators
          customStyle={calendarStyling}
          onDateSelect={(date) => this.setState({selectedDate: date})}
          eventDates={['2017-07-20', '2017-07-21', '2017-07-22']}
        />
        {
          this.state.selectedDate !== '' &&
          <Events
            selectedDate={this.state.selectedDate}
            events={MOCK_EVENTS}
            navigator={this.props.navigator}
          />
        }
      </View>
    );
  }
}

export default CalendarView;
