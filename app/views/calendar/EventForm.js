import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableHighlight,
  TextInput,
  PickerIOS,
  Switch
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 50
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  multilinetext: {
    marginTop: 10,
    fontSize: 18,
    padding: 4,
    borderWidth: 1,
    borderColor: '#48bbec',
    alignSelf: 'stretch',
    height: 100
  },
  input: {
    fontSize: 18,
    padding: 4,
    borderWidth: 1,
    borderColor: '#48bbec',
    height: 40,
    width: '48%'
  },
  button: {
    height: 50,
    backgroundColor: '#48bbec',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 22,
    color: '#ffffff',
    alignSelf: 'center'
  }
});

class EventForm extends Component {
  static propTypes = {
    selectedDate: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      project: 'p2',
      hours: 8,
      minutes: 0,
      description: '',
      overtime: false
    };
  }

  render() {
    const {
      project,
      hours,
      minutes,
      description,
      overtime
    } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.title}>
            {moment(this.props.selectedDate).format('dddd, MMM D YYYY')}
          </Text>
          <PickerIOS
            style={styles.projectpicker}
            selectedValue={project}
            onValueChange={(project) => this.setState({project})}
          >
            <PickerIOS.Item label="Project 1" value="p1" />
            <PickerIOS.Item label="Project 2" value="p2" />
            <PickerIOS.Item label="Project 3" value="p3" />
            <PickerIOS.Item label="Project 4" value="p4" />
            <PickerIOS.Item label="Project 5" value="p5" />
            <PickerIOS.Item label="Project 6" value="p6" />
            <PickerIOS.Item label="Project 7" value="p7" />
          </PickerIOS>
          <View
            style={{
              flexDirection: 'row',
              height: 50,
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <TextInput
              style={styles.input}
              keyboardType="numeric"
              placeholder="Hours"
              value={String(hours)}
              onChangeText={(hours) => this.setState({hours})}
            />
            <TextInput
              style={styles.input}
              keyboardType="numeric"
              placeholder="Minutes"
              value={String(minutes)}
              onChangeText={(minutes) => this.setState({minutes})}
            />
          </View>
          <TextInput
            multiline
            numberOfLines={4}
            editable
            style={styles.multilinetext}
            placeholder="Description"
            value={description}
            onChangeText={(description) => this.setState({description})}
          />
          <View
            style={{
              flexDirection: 'row',
              height: 50,
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <Text>Overtime</Text>
            <Switch
              value={overtime}
              onValueChange={(overtime) => this.setState({overtime})}
            />
          </View>
          <TouchableHighlight
            onPress={this.onClickSearch}
            style={styles.button}
          >
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableHighlight>
        </ScrollView>
      </View>
    );
  }
}

export default EventForm;
