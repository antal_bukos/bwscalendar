import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  StyleSheet,
  View,
  Text,
  ListView,
  TouchableHighlight
} from 'react-native';
import EventForm from './EventForm';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#BBBBBB',
    paddingTop: 10
  },
  header: {
    flexDirection: 'row',
    paddingBottom: 10
  },
  title: {
    flex: 1,
    fontSize: 15,
    paddingLeft: 10,
    fontWeight: 'bold'
  },
  addbutton: {
    paddingRight: 10
  },
  buttontext: {
    color: '#0e7afe',
    fontSize: 15
  },
  text: {
    fontSize: 15,
    color: '#333333'
  }
});

class Events extends Component {
  static propTypes = {
    events: PropTypes.array,
    selectedDate: PropTypes.string,
    navigator: PropTypes.object
  };

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      dataSource: ds.cloneWithRows(props.events)
    };
  }

  renderRow = (rowData) => {
    return (
      <View style={{
        backgroundColor: '#ffffff',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        alignItems: 'center',
        paddingBottom: 5,
        paddingTop: 5,
        borderBottomColor: '#BBBBBB'
      }}>
        <Text>{rowData.project}: {rowData.time_spent}</Text>
      </View>
    );
  };

  pressAdd = () => {
    this.props.navigator.push({
      title: 'Add Event',
      component: EventForm,
      passProps: {
        selectedDate: this.props.selectedDate
      }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>
            {moment(this.props.selectedDate).format('dddd, MMM D YYYY')}
          </Text>
          <TouchableHighlight
            style={styles.addbutton}
            onPress={this.pressAdd}
            underlayColor="#FFFFFF"
          >
            <Text style={styles.buttontext}>+ Add Event</Text>
          </TouchableHighlight>
        </View>
        <View style={{
          paddingBottom: 50,
          alignSelf: 'stretch',
          paddingLeft: 10,
          paddingRight: 10
        }}>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}
          />
        </View>
      </View>
    );
  }
}

export default Events;
