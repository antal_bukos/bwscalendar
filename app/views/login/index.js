import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import {GoogleSigninButton, GoogleSignin} from 'react-native-google-signin';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    paddingTop: 80,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class LoginView extends Component {
  static propTypes = {
    setUser: PropTypes.func.isRequired,
    clearUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
  }

  componentDidMount() {
    this._setupGoogleSignin();
  }

  _setupGoogleSignin = async () => {
    try {
      await GoogleSignin.hasPlayServices({autoResolve: true});
      await GoogleSignin.configure({
        iosClientId: '182569495093-kfnu0amndou01fbp510hcgedg1oio94o.apps.googleusercontent.com',
        offlineAccess: false
      });

      const user = await GoogleSignin.currentUserAsync();
      if (user) {
        this.props.setUser(user.email);
      } else {
        this.props.clearUser();
      }
    }
    catch(err) {
      console.log('Google signin error', err.code, err.message);
    }
  }

  _signIn = () => {
    GoogleSignin.signIn()
      .then((user) => {
        this.props.setUser(user.email);
      })
      .catch((err) => {
        console.log('WRONG SIGNIN', err);
      })
      .done(() => {
        console.log('sign in done');
      });
  }

  _signOut = () => {
    GoogleSignin.revokeAccess()
      .then(() => GoogleSignin.signOut()).then(() => {
        this.props.clearUser();
      });
  }

  render() {
    const {user} = this.props;
    if (user.isLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {  
          user.loggedIn ?
            <View>
              <Text>Logged in as: {user.email}</Text>
              <TouchableHighlight
                onPress={this._signOut}
              >
                <Text>Log out</Text>
              </TouchableHighlight>
            </View> :
            <View>
              <Text>Log in with google</Text>
              <GoogleSigninButton
                style={{width: 300, height: 50}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Light}
                onPress={this._signIn}
              />
            </View>
        }
      </View>
    );
  }
}

export default LoginView;
