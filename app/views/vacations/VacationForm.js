import React, {Component} from 'react';

import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableHighlight,
  TextInput,
  Switch,
  DatePickerIOS
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 50
  },
  multilinetext: {
    marginTop: 10,
    fontSize: 18,
    padding: 4,
    borderWidth: 1,
    borderColor: '#48bbec',
    alignSelf: 'stretch',
    height: 100
  },
  button: {
    height: 50,
    backgroundColor: '#48bbec',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 22,
    color: '#ffffff',
    alignSelf: 'center'
  }
});

class VacationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateFrom: new Date(),
      dateTo: new Date(),
      reason: '',
      paid: false
    };
  }

  render() {
    const {
      dateFrom,
      dateTo,
      reason,
      paid
    } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={{
            alignSelf: 'center',
            fontSize: 15
          }}>From</Text>
          <DatePickerIOS
            onDateChange={(dateFrom) => this.setState({dateFrom})}
            mode="date"
            date={dateFrom}
          />
          <Text style={{
            alignSelf: 'center',
            fontSize: 15
          }}>To</Text>
          <DatePickerIOS
            onDateChange={(dateTo) => this.setState({dateTo})}
            mode="date"
            date={dateTo}
          />
          <TextInput
            multiline
            numberOfLines={4}
            editable
            style={styles.multilinetext}
            placeholder="Reason"
            value={reason}
            onChangeText={(reason) => this.setState({reason})}
          />
          <View
            style={{
              flexDirection: 'row',
              height: 50,
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <Text>Paid</Text>
            <Switch
              value={paid}
              onValueChange={(paid) => this.setState({paid})}
            />
          </View>
          <TouchableHighlight
            onPress={this.onClickSearch}
            style={styles.button}
          >
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableHighlight>
        </ScrollView>
      </View>
    );
  }
}

export default VacationForm;
