import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  StyleSheet,
  View,
  Text,
  ListView
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingTop: 10
  }
});

class Vacations extends Component {
  static propTypes = {
    vacations: PropTypes.array,
    selectedView: PropTypes.string
  };

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      dataSource: ds.cloneWithRows(props.vacations)
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.vacations)
    });
  }

  renderRow = (rowData) => {
    return (
      <View style={{
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        paddingBottom: 5,
        paddingTop: 5,
        borderBottomColor: '#BBBBBB'
      }}>
        <Text>
          <Text style={{fontWeight: 'bold'}}>From: </Text>
          {moment(rowData.from).format('dddd, MMM D YYYY')}
        </Text>
        <Text>
          <Text style={{fontWeight: 'bold'}}>To: </Text>
          {moment(rowData.to).format('dddd, MMM D YYYY')}
        </Text>
        <Text>
          <Text style={{fontWeight: 'bold'}}>Days: </Text>
          {rowData.daysCount}
        </Text>
        <Text>
          <Text style={{fontWeight: 'bold'}}>Reason: </Text>
          {rowData.reason || '-'}
        </Text>
      </View>
    );
  };

  render() {
    const {
      selectedView,
      vacations
    } = this.props;

    return (
      <View style={styles.container}>
        {
          vacations.length === 0 ?
            <Text>No {selectedView} requests</Text> :
            <ListView
              enableEmptySections
              dataSource={this.state.dataSource}
              renderRow={this.renderRow}
            />
        }
      </View>
    );
  }
}

export default Vacations;
