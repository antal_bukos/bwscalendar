import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  SegmentedControlIOS,
  TouchableHighlight
} from 'react-native';

import VacationForm from './VacationForm';
import Vacations from './Vacations';

const styles = StyleSheet.create({
  container: {
    paddingTop: 80,
    paddingLeft: 20,
    paddingRight: 20
  },
  title: {
    alignSelf: 'center',
    fontSize: 15
  },
  addbutton: {
    alignSelf: 'center',
    paddingBottom: 10
  },
  buttontext: {
    color: '#0e7afe',
    fontSize: 15
  }
});

const viewOptions = ['Pending', 'Approved', 'Declined'];
const vacations = [
  [],
  [{
    from: '2017-08-14',
    to: ' 2017-08-18',
    daysCount: 5,
    reason: 'why not?'
  }, {
    from: '2016-01-01',
    to: '2016-01-10',
    daysCount: 10
  }],
  [{
    from: '2017-07-14',
    to: ' 2017-07-18',
    daysCount: 5,
    reason: 'get back to work'
  }]
];

class VacationsView extends Component {
  static propTypes = {
    navigator: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedView: 0
    };
  }

  pressAdd = () => {
    this.props.navigator.push({
      title: 'Request Vacation',
      component: VacationForm
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Paid vacation days left: 365</Text>
        <TouchableHighlight
          style={styles.addbutton}
          onPress={this.pressAdd}
          underlayColor="#FFFFFF"
        >
          <Text style={styles.buttontext}>+ Request Vacation</Text>
        </TouchableHighlight>
        <Text style={{
          alignSelf: 'center',
          fontWeight: 'bold',
          paddingBottom: 10,
          fontSize: 15
        }}>My requests</Text>
        <SegmentedControlIOS
          values={viewOptions}
          selectedIndex={this.state.selectedView}
          onChange={(event) => {
            this.setState({
              selectedView: event.nativeEvent.selectedSegmentIndex
            });
          }}
        />
        <Vacations
          selectedView={viewOptions[this.state.selectedView]}
          vacations={vacations[this.state.selectedView]}
        />
      </View>
    );
  }
}

export default VacationsView;
