/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
  AppRegistry
} from 'react-native';
import {createLogger} from 'redux-logger';
import {Provider} from 'react-redux';
import {
  createStore,
  applyMiddleware,
  compose
} from 'redux';
import thunkMiddleware from 'redux-thunk';

import reducer from './app/reducers';
import CalendarApp from './app/main';

const loggerMiddleware = createLogger(
  {collapsed: true}
);

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware, // lets us dispatch() functions
      loggerMiddleware,
    ),
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

export default class BWSCalendar extends Component {
  render() {
    return (
      <Provider store={store}>
        <CalendarApp />
      </Provider>
    );
  }
}


AppRegistry.registerComponent('BWSCalendar', () => BWSCalendar);
